class docker {

  $antigos = ["docker", "docker-engine", "docker-io"]

  $novos = ["apt-transport-https", "ca-certificates", "curl", "software-properties-common"]

  package { $antigos:
    ensure => "absent",
  }
->
  package { $novos:
    ensure => "present",
  }
->
  exec {"add_repo":
    command => "/usr/bin/curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -"
  }
->
  exec {"add_apt_repo":
    command => "/usr/bin/add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\""
  }
->
  exec {"update_repo":
    command => "/usr/bin/apt-get update"
  }
->
  package {"docker-ce":
   ensure => "present",
  }
}
