class web {
  package { "apache2":
    ensure => 'absent'
  }

  package { "nginx" :
    ensure => 'present'
  }

  file { "/var/www/html/index.html":
    source => "puppet:///modules/web/index.html",
    ensure => 'present',
  }

  service { "nginx":
    ensure => "running",
    enable => "true",
  }

}
