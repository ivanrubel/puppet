class config {

  file {"/var/www/html/index.html":
    source => "/var/www/html/index.html",
    ensure => present
  }

  package { "apache2":
    ensure => 'absent'}

  package { "nginx":
    ensure => 'present'
  }
}
